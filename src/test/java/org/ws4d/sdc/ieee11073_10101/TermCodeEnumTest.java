package org.ws4d.sdc.ieee11073_10101;

import static java.lang.Boolean.valueOf;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.ws4d.sdc.ieee11073_10101.Code;
import org.ws4d.sdc.ieee11073_10101.PartitionCode;
import org.ws4d.sdc.ieee11073_10101.TermCode;
import org.ws4d.sdc.ieee11073_10101.partition.BodySite;
import org.ws4d.sdc.ieee11073_10101.partition.CommunicationInfrastructure;
import org.ws4d.sdc.ieee11073_10101.partition.Dimension;
import org.ws4d.sdc.ieee11073_10101.partition.Event;
import org.ws4d.sdc.ieee11073_10101.partition.ExternalNomenclature;
import org.ws4d.sdc.ieee11073_10101.partition.MedicalSupervisoryControlAndDataAcquisition;
import org.ws4d.sdc.ieee11073_10101.partition.ObjectInfrastructure;
import org.ws4d.sdc.ieee11073_10101.partition.ParameterGroup;
import org.ws4d.sdc.ieee11073_10101.partition.ProprietaryExtension;
import org.ws4d.sdc.ieee11073_10101.partition.Unspecified;
import org.ws4d.sdc.ieee11073_10101.partition.VirtualAttribute;

public class TermCodeEnumTest {

	private static List<Class<? extends Enum<? extends Code>>> list = new ArrayList<>();

	@BeforeClass
	public static void setUp() throws Exception {
		list.add(BodySite.class);
		list.add(CommunicationInfrastructure.class);
		list.add(Dimension.class);
		list.add(Event.class);
		list.add(ExternalNomenclature.class);
		list.add(MedicalSupervisoryControlAndDataAcquisition.class);
		list.add(ObjectInfrastructure.class);
		list.add(ParameterGroup.class);
		list.add(ProprietaryExtension.class);
		list.add(Unspecified.class);
		list.add(VirtualAttribute.class);
	}

	@SuppressWarnings("static-method")
	@Test
	public void testInvariantEnumTermcodeEnum() throws Exception {
		for (Class<? extends Enum<? extends Code>> c : TermCodeEnumTest.list) {
			for (Enum<? extends Code> original : c.getEnumConstants()) {
				Code originalCode = (Code) original;
				PartitionCode partitionCode = originalCode.getPartitionCode();
				int termCode = originalCode.asTermCode();
				Code a = new TermCode(termCode, partitionCode);
				assertEquals(valueOf(a.isEquivalent(originalCode)), valueOf(true));
			}
		}
	}

	@SuppressWarnings("static-method")
	@Test
	public void testInvariantEnumNomenclatureCodeEnum() throws Exception {
		for (Class<? extends Enum<? extends Code>> c : TermCodeEnumTest.list) {
			for (Enum<? extends Code> original : c.getEnumConstants()) {
				Code originalCode = (Code) original;
				int nomenclatureCode = originalCode.asNomenclatureCode();
				Code b = TermCode.fromNomenclatureCode(nomenclatureCode);
				assertEquals(valueOf(b.isEquivalent(originalCode)), valueOf(true));
			}
		}
	}

	@SuppressWarnings("static-method")
	@Test
	public void testInvariantFromTermcodeEqualsFromNomenclatureCode() throws Exception {
		for (Class<? extends Enum<? extends Code>> c : TermCodeEnumTest.list) {
			for (Enum<? extends Code> original : c.getEnumConstants()) {
				Code originalCode = (Code) original;
				PartitionCode partitionCode = originalCode.getPartitionCode();
				int nomenclatureCode = originalCode.asNomenclatureCode();
				int termCode = originalCode.asTermCode();
				Code a = new TermCode(termCode, partitionCode);
				Code b = TermCode.fromNomenclatureCode(nomenclatureCode);
				assertEquals(valueOf(a.isEquivalent(b)), valueOf(true));
			}
		}
	}


	@SuppressWarnings("static-method")
	@Test
	public void testNomenclatureCodeLowInnerBorder() throws Exception {
		Code a = TermCode.fromNomenclatureCode(0);
		assertEquals(valueOf(a.isEquivalent(Unspecified.MDC_UNSPEC)), valueOf(true));
	}

	@SuppressWarnings("static-method")
	@Test(expected = Exception.class)
	public void testNomenclatureCodeLowOuterBorder() throws Exception {
		TermCode.fromNomenclatureCode(-1);
	}

	@SuppressWarnings("static-method")
	@Test
	public void testNomenclatureCodeHighInnerBorder() throws Exception {
		int max = (int) ((1024 << 16) + Math.pow(2, 16)) - 1;
		Code a = TermCode.fromNomenclatureCode(max);
		assertEquals(valueOf(PartitionCode.MDC_PART_PVT.equals(a.getPartitionCode())), valueOf(true));
		assertEquals(valueOf(a.asTermCode() == 65535), valueOf(true));
	}

	@SuppressWarnings("static-method")
	@Test(expected = Exception.class)
	public void testNomenclatureCodeHighOuterBorder() throws Exception {
		int tooMuch = (1024 << 16) + (2 << 16);
		TermCode.fromNomenclatureCode(tooMuch);
	}
}