package org.ws4d.sdc.ieee11073_10101;

import java.util.ArrayList;
import java.util.List;

import org.ws4d.sdc.ieee11073_10101.partition.BodySite;
import org.ws4d.sdc.ieee11073_10101.partition.CommunicationInfrastructure;
import org.ws4d.sdc.ieee11073_10101.partition.Dimension;
import org.ws4d.sdc.ieee11073_10101.partition.Event;
import org.ws4d.sdc.ieee11073_10101.partition.ExternalNomenclature;
import org.ws4d.sdc.ieee11073_10101.partition.MedicalSupervisoryControlAndDataAcquisition;
import org.ws4d.sdc.ieee11073_10101.partition.ObjectInfrastructure;
import org.ws4d.sdc.ieee11073_10101.partition.ParameterGroup;
import org.ws4d.sdc.ieee11073_10101.partition.ProprietaryExtension;
import org.ws4d.sdc.ieee11073_10101.partition.Unspecified;
import org.ws4d.sdc.ieee11073_10101.partition.VirtualAttribute;

/**
 * Implementation of IEEE 11073-10101:2005<br>
 * In the documentation of this class terms are used as follows: TermCode = a
 * specific entity of this class<br>
 * termcode = a context sensitive integer representation.<br>
 * nomenclature code = a context free integer representation.
 * 
 * @author Björn Butzin<bjoern.butzin@uni-rostock.de>
 */
public class TermCode implements Code {

	/** The [contextsensitive] termcode **/
	private int contextInsensitiveCode;

	/** The partition code (block code) to specify the context for the termcode **/
	private PartitionCode partitionCode;

	/**
	 * Obtains the TermCode member for a given [context-sensitive] termcode in
	 * conjunction with the partition code the termcode belongs to. This can be used
	 * to convert integer values to the correct TermCode. If a context free
	 * nomenclature code is provided as input or a nuber that cannot be a termcode
	 * an excetion is thrown.
	 * 
	 * @param termCode      an integer supposed to be a termcode code
	 * @param partitionCode the partition code the termcode shall belong to
	 */
	public TermCode(int termCode, PartitionCode partitionCode) {
		if (TermCode.isTermCode(termCode)) {
			this.contextInsensitiveCode = termCode;
			this.partitionCode = partitionCode;
		} else {
			this.contextInsensitiveCode = Unspecified.MDC_UNSPEC.asTermCode();
			this.partitionCode = Unspecified.partitionCode;
		}
	}

	@Override
	public String toString() {
		Code match;
		switch (this.partitionCode) {
		case MDC_PART_UNSPEC:
			return Unspecified.MDC_UNSPEC.toString();
		case MDC_PART_OBJ:
			match = ObjectInfrastructure.fromContextInsensitiveCode(this.contextInsensitiveCode);
			break;
		case MDC_PART_SCADA:
			match = MedicalSupervisoryControlAndDataAcquisition.fromContextInsensitiveCode(this.contextInsensitiveCode);
			break;
		case MDC_PART_EVT:
			match = Event.fromContextInsensitiveCode(this.contextInsensitiveCode);
			break;
		case MDC_PART_DIM:
			match = Dimension.fromContextInsensitiveCode(this.contextInsensitiveCode);
			break;
		case MDC_PART_VATTR:
			match = VirtualAttribute.fromContextInsensitiveCode(this.contextInsensitiveCode);
			break;
		case MDC_PART_PGRP:
			match = ParameterGroup.fromContextInsensitiveCode(this.contextInsensitiveCode);
			break;
		case MDC_PART_SITES:
			match = BodySite.fromContextInsensitiveCode(this.contextInsensitiveCode);
			break;
		case MDC_PART_INFRA:
			match = CommunicationInfrastructure.fromContextInsensitiveCode(this.contextInsensitiveCode);
			break;
		case MDC_PART_EXT_NOM:
			match = ExternalNomenclature.fromContextInsensitiveCode(this.contextInsensitiveCode);
			break;
		case MDC_PART_PVT:
			match = ProprietaryExtension.fromContextInsensitiveCode(this.contextInsensitiveCode);
			break;
		default:
			return "" + this.asNomenclatureCode();
		}
		if (match != Unspecified.MDC_UNSPEC) {
			return match.toString();
		}
		return "" + this.asNomenclatureCode();
	}

	public int asTermCode() {
		return this.contextInsensitiveCode;
	}

	public PartitionCode getPartitionCode() {
		return this.partitionCode;
	}

	@SuppressWarnings("static-method")
	public Code fromContextInsensitiveCode(@SuppressWarnings("unused") int termCode) {
		return Unspecified.MDC_UNSPEC;
	}

	/**
	 * Obtains the TermCode member for a given [context-free] nomenclature code.
	 * This can be used to convert integer values to the correct TermCode. If a
	 * context sensitive termcode is provided as input or a nuber that cannot be a
	 * nomenclature code an excetion is thrown.
	 * 
	 * @param nomenclatureCode an integer supposed to be a nomenclature code
	 * @return The termcode matching the given nomenclature code
	 * @throws Exception if the provided nomenclature code is either an context
	 *                   sensitive termcode or an arbitrary number that cannot be a
	 *                   nomenclature code
	 */
	public static Code fromNomenclatureCode(int nomenclatureCode) throws Exception {
		if (isNomenclatureCode(nomenclatureCode)) {
			PartitionCode partitionCode = PartitionCode.fromInt(nomenclatureCode >> 16);
			int termCode = (int) (nomenclatureCode - partitionCode.asCode() * Math.pow(2, 16));
			return new TermCode(termCode, partitionCode);
		}
		throw new Exception(nomenclatureCode + " is not a valid IEEE 11073-10101 nomenclature code");
	}

	/**
	 * Checks if the given code is in range to be a valid termcode. A return value
	 * of 'true' does not guarantee that this termcode is actually known or used.
	 * 
	 * @param code the integer that shall be checked
	 * @return true if the given code is in range to be a termcode (without
	 *         guarantee to actually be an assigned TermCode), false otherwise.
	 */
	public static boolean isTermCode(int code) {
		if (code >= 0) {
			return code < Math.pow(2, 16);
		}
		return false;
	}

	/**
	 * Checks if the given code is in range to be a valid nomenclature code. A
	 * return value of 'true' does not guarantee that this nomenclature code is
	 * actually known or used.
	 * 
	 * @param code the integer that shall be checked
	 * @return true if the given code is in range to be a nomenclature code (without
	 *         guarantee to actually be an assigned TermCode), false otherwise.
	 */
	public static boolean isNomenclatureCode(int code) {
		if (0 <= code && code < PartitionCode.MDC_PART_PVT.asCode() * Math.pow(2, 16) + Math.pow(2, 16)) {
			return code == 0 || !isTermCode(code);
		}
		return false;
	}

	/**
	 * Obtains a TermCode from its string representation
	 * 
	 * @param termCodeString the string supposed to present a termcode
	 * @return The TermCode that matches exactly the string provided or
	 *         {@link Unspecified#MDC_UNSPEC} otherwise.
	 */
	public static Code fromString(String termCodeString) {
		String v = termCodeString.toUpperCase();
		List<Class<? extends Enum<? extends Code>>> list = new ArrayList<>();
		list.add(BodySite.class);
		list.add(CommunicationInfrastructure.class);
		list.add(Dimension.class);
		list.add(Event.class);
		list.add(ExternalNomenclature.class);
		list.add(MedicalSupervisoryControlAndDataAcquisition.class);
		list.add(ObjectInfrastructure.class);
		list.add(ParameterGroup.class);
		list.add(ProprietaryExtension.class);
		list.add(Unspecified.class);
		list.add(VirtualAttribute.class);

		for (Class<? extends Enum<? extends Code>> c : list) {
			for (Enum<? extends Code> g : c.getEnumConstants()) {
				if (g.toString().toUpperCase().equals(v))
					return (Code) g;
			}
		}
		return Unspecified.MDC_UNSPEC;
	}

	public static int asNomenclatureCode(int termCode, PartitionCode partitionCode) {
		return (int) (partitionCode.asCode() * Math.pow(2, 16) + termCode);
	}
}
