package org.ws4d.sdc.ieee11073_10101;

/**
 * Discriminator ranges [Ref. IEEE 11073-10101 B.1.3]<br>
 * Discriminators are organized so that codes for homologous discriminators are
 * all located within the range specified. In this listing, the Discrim Offset
 * indicates the common denominator of the discriminator (i.e., the numerical
 * difference between discriminator codes), which in binary amounts to
 * log2(DiscrimOffset); e.g., ECG Lead discriminator is log2(256)=8 bits).
 **/
public enum DescriminatorRange {

	/* Discriminator_Range_ Definitions */
	/* Sup'y Control and Data Acq'n (SCADA) CodeBlock: 2 */
	/* ECG Lead Discrim Offset: 256 */
	MDC_DRANGE_ECG_LEAD_START(0), MDC_DRANGE_ECG_LEAD_END(16127),
	/* ECG Patterns Discrim Offset: 8 */
	MDC_DRANGE_ECG_PATT_START(16448), MDC_DRANGE_ECG_PATT_END(17999),
	/* Pulsatile - Hemo Discrim Offset: 4 */
	MDC_DRANGE_PULS_HEMO_START(18944), MDC_DRANGE_PULS_HEMO_END(19219),
	/* Pulsatile - Neuro Discrim Offset: 4 */
	MDC_DRANGE_PULS_NEURO_START(22532), MDC_DRANGE_PULS_NEURO_END(22655);

	private final int value;

	DescriminatorRange(int value) {
		this.value = value;
	}

	public static DescriminatorRange fromInt(int i) {
		for (DescriminatorRange g : DescriminatorRange.values()) {
			if (g.value == i)
				return g;
		}
		return null;
	}

	public static DescriminatorRange fromString(String s) {
		String v = s.toUpperCase();
		for (DescriminatorRange g : DescriminatorRange.values()) {
			if (g.toString().toUpperCase().equals(v))
				return g;
		}
		return null;
	}

	public int asCode() {
		return this.value;
	}
}
