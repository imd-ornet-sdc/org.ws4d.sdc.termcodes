package org.ws4d.sdc.ieee11073_10101;

public interface Code {
	
	/**
	 * @return The plain context sensitive termcode that is represented by this
	 *         TermCode.
	 */
	int asTermCode();

	/**
	 * @return The partition code the TermCode belongs to.
	 */
	PartitionCode getPartitionCode();

	/**
	 * @return The fully context free nomenclature code representing this TermCode.
	 */
	default int asNomenclatureCode() {
		return (int) (this.getPartitionCode().asCode() * Math.pow(2, 16) + this.asTermCode());
	}
	
	/**
	 * Checks if this TermCodes is semantically equivalent to the other, i.e even if
	 * they have a different string representation, their termcode and partition
	 * code is equal.
	 * 
	 * @param other the TermCode to compare with
	 * @return true if both codes are equivalent.
	 */
	default boolean isEquivalent(Code other) {
		return other.asTermCode() == this.asTermCode() && other.getPartitionCode() == this.getPartitionCode();
	}
}
