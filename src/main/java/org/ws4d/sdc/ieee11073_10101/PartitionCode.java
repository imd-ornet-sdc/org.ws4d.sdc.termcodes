package org.ws4d.sdc.ieee11073_10101;

/**
 * Partition codes (or code block) [Ref. IEEE 11073-10101 B.1.2]<br>
 * Partition codes may be considered to be unique as the high-order 16 bits of a
 * 32-bit integer code.
 **/
public enum PartitionCode {

	/** Unspecified */
	MDC_PART_UNSPEC(0),

	/** Object Infrastructure */
	MDC_PART_OBJ(1),

	/** Medical supervisory control and data acquisition (SCADA) (Physio IDs) */
	MDC_PART_SCADA(2),

	/** Event */
	MDC_PART_EVT(3),

	/** Dimension */
	MDC_PART_DIM(4),

	/** Virtual Attribute */
	MDC_PART_VATTR(5),

	/** Parameter Group */
	MDC_PART_PGRP(6),

	/** [Body] Site */
	MDC_PART_SITES(7),

	/** [Communication] Infrastructure */
	MDC_PART_INFRA(8),

	/** File Exchange Format */
	MDC_PART_FEF(9),

	/** External Nomenclature */
	MDC_PART_EXT_NOM(256),

	/** Private */
	MDC_PART_PVT(1024);

	/** The integer representation of the partition code */
	private final int partitionCode;

	PartitionCode(int partitionCode) {
		this.partitionCode = partitionCode;
	}

	public static PartitionCode fromInt(int i) {
		for (PartitionCode g : PartitionCode.values()) {
			if (g.partitionCode == i)
				return g;
		}
		return PartitionCode.MDC_PART_UNSPEC;
	}

	public static PartitionCode fromString(String s) {
		String v = s.toUpperCase();
		for (PartitionCode g : PartitionCode.values()) {
			if (g.toString().toUpperCase().equals(v))
				return g;
		}
		return PartitionCode.MDC_PART_UNSPEC;
	}

	public int asCode() {
		return this.partitionCode;
	}
}
