package org.ws4d.sdc.ieee11073_10101.partition;

import org.ws4d.sdc.ieee11073_10101.Code;
import org.ws4d.sdc.ieee11073_10101.PartitionCode;

public enum Unspecified implements Code {
	MDC_UNSPEC(0),
	;

	/** The [contextsensitive] termcode **/
	public final int contextSensitiveCode;

	/** The partition code (block code) to specify the context for the termcode **/
	public static final PartitionCode partitionCode = PartitionCode.MDC_PART_UNSPEC;

	Unspecified(int contextSensitiveCode) {
		this.contextSensitiveCode = contextSensitiveCode;
	}

	public static Code fromContextInsensitiveCode(@SuppressWarnings("unused") int termCode) {
		return Unspecified.MDC_UNSPEC;
	}

	public int asTermCode() {
		return this.contextSensitiveCode;
	}

	public PartitionCode getPartitionCode() {
		return partitionCode;
	}
}
