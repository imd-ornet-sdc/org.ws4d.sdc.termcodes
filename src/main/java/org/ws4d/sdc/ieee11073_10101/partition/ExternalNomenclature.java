package org.ws4d.sdc.ieee11073_10101.partition;

import org.ws4d.sdc.ieee11073_10101.Code;
import org.ws4d.sdc.ieee11073_10101.PartitionCode;

/** IEEE 11073-10101 B.10 External nomenclature */
public enum ExternalNomenclature implements Code {

	// Partition: EXTnom Description External Nomenclature
	MDC_EXT_NOM_SNOMED(1), /* SNOMED */
	MDC_EXT_NOM_UMLS(64), /* UMLS */
	MDC_EXT_NOM_MeSH(128), /* MeSH */
	MDC_EXT_NOM_LOINC(192), /* LOINC */
	MDC_EXT_NOM_HL7(256), /* HL7 */
	MDC_EXT_NOM_READ(320), /* READ */
	MDC_EXT_NOM_ICD_9(384), /* ICD-9-CM */
	MDC_EXT_NOM_ICD_10(385), /* ICD–10 */
	MDC_EXT_NOM_NNN(448), /* NNN-Code */
	MDC_EXT_NOM_MC(512), /* MC (Minnesota) */
	MDC_EXT_NOM_SCP(576), /* SCP */
	MDC_EXT_NOM_NIC(640), /* NIC */
	MDC_EXT_NOM_NOC(704), /* NOC */
	MDC_EXT_NOM_ICPM(768), /* ICPM */
	MDC_EXT_NOM_ICPM_GE(832), /* ICPM-GE */
	MDC_EXT_NOM_VESKA(896), /* VESKA */
	MDC_EXT_NOM_ASTM_E1394_91(960), /* ASTM E1394-91 */
	MDC_EXT_NOM_ASTM_E1238(1024), /* ASTM E1238 */
	MDC_EXT_NOM_DSM_IIIR(1088), /* DSM-IIIR */
	MDC_EXT_NOM_DRG(1152), /* DRG */
	MDC_EXT_NOM_NANDA(1216), /* NANDA */
	MDC_EXT_NOM_GALEN(1280), /* GALEN */
	MDC_EXT_NOM_GRAIL(1344), /* GRAIL */
	MDC_EXT_NOM_ASTM_E1467_94(1408), /* ASTM E1467-94 */
	MDC_EXT_NOM_CPT(1472), /* CPT */
	MDC_EXT_NOM_OPCS_4(1536), /* OPCS-4 */
	MDC_EXT_NOM_ASTM_E1460_92(1600), /* ASTM E1460-92 (Arden) */
	;
	
	/** The [contextsensitive] termcode **/
	public final int contextSensitiveCode;

	/** The partition code (block code) to specify the context for the termcode **/
	public static final PartitionCode partitionCode = PartitionCode.MDC_PART_EXT_NOM;

	ExternalNomenclature(int contextSensitiveCode) {
		this.contextSensitiveCode = contextSensitiveCode;
	}

	public static Code fromContextInsensitiveCode(int termCode) {
		// known
		for (Code v : ExternalNomenclature.values()) {
			if (v.asTermCode() == termCode) return v;
		}
		// unknown
		return Unspecified.MDC_UNSPEC;
	}

	public int asTermCode() {
		return this.contextSensitiveCode;
	}

	public PartitionCode getPartitionCode() {
		return partitionCode;
	}
}