package org.ws4d.sdc.ieee11073_10101.partition;

import org.ws4d.sdc.ieee11073_10101.Code;
import org.ws4d.sdc.ieee11073_10101.PartitionCode;

/** IEEE 11073-10101 B.7 Parameter groups */
public enum ParameterGroup implements Code {

	MDC_PGRP_HEMO(513), /* 1844 */
	MDC_PGRP_ECG(514), /* 1845 */
	MDC_PGRP_RESP(515), /* 1846 */
	MDC_PGRP_VENT(516), /* 1847 */
	MDC_PGRP_NEURO(517), /* 1848 */
	MDC_PGRP_DRUG(518), /* 1851 */
	MDC_PGRP_FLUID(519), /* 1850 */
	MDC_PGRP_BLOOD_CHEM(520), 
	MDC_PGRP_MISC(521), 
	;
	
	/** The [contextsensitive] termcode **/
	public final int contextSensitiveCode;

	/** The partition code (block code) to specify the context for the termcode **/
	public static final PartitionCode partitionCode = PartitionCode.MDC_PART_PGRP;

	ParameterGroup(int contextSensitiveCode) {
		this.contextSensitiveCode = contextSensitiveCode;
	}

	public static Code fromContextInsensitiveCode(int termCode) {
		// known
		for (Code v : ParameterGroup.values()) {
			if (v.asTermCode() == termCode) return v;
		}
		// unknown
		return Unspecified.MDC_UNSPEC;
	}

	public int asTermCode() {
		return this.contextSensitiveCode;
	}

	public PartitionCode getPartitionCode() {
		return partitionCode;
	}
}