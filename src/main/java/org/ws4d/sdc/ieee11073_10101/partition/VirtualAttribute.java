package org.ws4d.sdc.ieee11073_10101.partition;

import org.ws4d.sdc.ieee11073_10101.Code;
import org.ws4d.sdc.ieee11073_10101.PartitionCode;

/** IEEE 11073-10101 B.6 Virtual attributes */
public enum VirtualAttribute implements Code {

	/* <This subclause remains to be defined> */
	;
	
	/** The [contextsensitive] termcode **/
	public final int contextSensitiveCode;

	/** The partition code (block code) to specify the context for the termcode **/
	public static final PartitionCode partitionCode = PartitionCode.MDC_PART_VATTR;

	VirtualAttribute(int contextSensitiveCode) {
		this.contextSensitiveCode = contextSensitiveCode;
	}

	public static Code fromContextInsensitiveCode(int termCode) {
		// known
		for (Code v : VirtualAttribute.values()) {
			if (v.asTermCode() == termCode) return v;
		}
		// unknown
		return Unspecified.MDC_UNSPEC;
	}

	public int asTermCode() {
		return this.contextSensitiveCode;
	}

	public PartitionCode getPartitionCode() {
		return partitionCode;
	}
}